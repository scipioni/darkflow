# darkflow

## install

1) make python virtualenv
```
make virtualenv
```

2) build tensorflow from https://gitlab.com/scipioni/tensorflow.git and install
```
pip install /opt/tensorflow/tensorflow-1.7.1-cp36-cp36m-linux_x86_64.whl
```

3) install darkflow
```
make darkflow
```


test with CPU
```
make test-cpu
```

test with GPU
```
make test-gpu
```


## create dataset

for example create dataset fruit to recognize apple and lemon (2 classes)
```
mkdir -p cfg
cp ./darkflow.git/cfg/tiny-yolo-voc.cfg cfg/fruit.cfg
```

edit cfg/fruit.cfg in last layer (region)
```
classes=2
```

in last layer there is num=5

edit cfg/fruit.cfg in second last layer with filters=num*(classes+5)
```
filters=35
```

create cfg/fruit.labels with labels
```
lemon
apple
```
