import tensorflow as tf

graph_filename = "built_graph/tiny-yolo-voc.pb"
out_graph_filename = "built_graph/tiny-yolo-voc.pbtxt"

with tf.gfile.GFile(graph_filename, "rb") as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
    tf.train.write_graph(graph_def, './', out_graph_filename, True)

