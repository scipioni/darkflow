mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(shell dirname $(mkfile_path))
export PATH := $(current_dir)/lib/bin:$(PATH)

darkflow: darkflow.git/README.md weights

darkflow.git/README.md:
	git clone https://github.com/thtrieu/darkflow.git darkflow.git
	bash -c "cd darkflow.git && git apply ../darkflow.patch"
	bash -c "cd darkflow.git && ../lib/bin/pip install ."
	
diff:
	bash -c "cd darkflow.git && git diff > ../darkflow.patch"

virtualenv: lib/bin/activate

lib/bin/activate:
	python3 -mvenv lib
	lib/bin/pip install -U pip wheel
	lib/bin/pip install -U cython
	lib/bin/pip install -U opencv-python
	lib/bin/pip install -U pascal-voc-writer

data:
	mkdir data

weigths: data data/yolov2-tiny-voc.weights

data/yolov2-tiny-voc.weights:
	bash -c "wget https://pjreddie.com/media/files/yolov2-tiny-voc.weights -O data/yolov2-tiny-voc.weights"

download-voc: data/VOCtest_06-Nov-2007.tar data/VOCdevkit/VOC2007/Annotations

data/VOCtest_06-Nov-2007.tar:
	wget --directory-prefix=data https://pjreddie.com/media/files/VOCtest_06-Nov-2007.tar

data/VOCdevkit/VOC2007/Annotations:
	bash -c "cd data && tar xf data/VOCtest_06-Nov-2007.tar"

all: virtualenv darkflow weights

test-cpu:
	flow --model darkflow.git/cfg/tiny-yolo-voc.cfg  --load data/yolov2-tiny-voc.weights --demo camera

test-gpu:
	flow --model darkflow.git/cfg/tiny-yolo-voc.cfg  --load data/yolov2-tiny-voc.weights --demo camera --gpu 1.0


.PHONY: labelImg
labelImg: /usr/share/doc/pyqt5-dev-tools/changelog.Debian.gz labelImg.git/README.rst lib/bin/labelImg

/usr/share/doc/pyqt5-dev-tools/changelog.Debian.gz:
	sudo apt-get install pyqt5-dev-tools

labelImg.git/README.rst:
	git clone --depth 1 https://github.com/tzutalin/labelImg.git labelImg.git 
	pip install lxml
	pip install PyQt5
	bash -c "cd labelImg.git && make qt5py3"

lib/bin/labelImg:
	@echo "#!/bin/sh" > lib/bin/labelImg
	@echo "cd $(current_dir)/labelImg.git && exec python labelImg.py" >> lib/bin/labelImg
	@chmod +x lib/bin/labelImg


capture:
	mkdir -p dataset/fruit
	bash -c "sleep 2 && ffmpeg -y -f v4l2 -framerate 1 -video_size 640x480 -input_format yuyv422 -i /dev/video0 -vframes 20 -q:v 1 dataset/fruit/%3d.jpg"

gmonitor: lib/bin/gmonitor

lib/bin/gmonitor: gmonitor.git
	bash -c "cd gmonitor.git/src && make && cp gmonitor ../../lib/bin"	

gmonitor.git:
	git clone https://github.com/mountassir/gmonitor.git gmonitor.git
	

train:
	flow --model cfg/fruit.cfg --load data/yolov2-tiny-voc.weights --train --dataset dataset/fruit --annotation dataset/fruit --labels cfg/fruit.labels --gpu 1.0

pb:
	flow --model cfg/fruit.cfg --load data/yolov2-tiny-voc.weights --labels cfg/fruit.labels --savepb

pb-voc: built_graph/tiny-yolo-voc.pb

built_graph/tiny-yolo-voc.pb:
	flow --savepb --verbalise --model darkflow.git/cfg/tiny-yolo-voc.cfg --load data/yolov2-tiny-voc.weights --labels cfg/voc.names

test-fruit-cpu:
	flow --pbLoad ./built_graph/fruit.pb --metaLoad ./built_graph/fruit.meta --demo camera

test-voc:
	flow --pbLoad ./built_graph/tiny-yolo-voc.pb --metaLoad ./built_graph/tiny-yolo-voc.meta --demo camera
